
VERSION=1.0

.PHONY: build

build:
	go build -ldflags="-X github.com/prometheus/common/version.Version=${VERSION} \
			-X github.com/prometheus/common/version.Revision=0 \
			-X github.com/prometheus/common/version.Branch=main \
			-X github.com/prometheus/common/version.BuildDate=`date +%F`"

install:
	install obgpd_exporter /usr/local/bin
	install obgpd_exporter.rc /etc/rc.d/obgpd_exporter

clean:
	rm obgpd_exporter
