package main

import (
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os/exec"
	"os/user"
	"strings"
	"time"

	"github.com/go-kit/log/level"
	"inet.af/netaddr"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/version"
	"github.com/prometheus/common/promlog"
)

const namespace = "obgpd"

type Exporter struct { }

type bgpCtl struct {
	Neighbors []bgpNeighbors `json:"neighbors"`
}

type bgpNeighbors struct {
	RemoteAS string `json:"remote_as"`
	Description string `json:"description,omitempty"`
	Group string `json:"group,omitempty"`
	RemoteAddr string `json:"remote_addr"`
	BGPID string `json:"bgpid,omitempty"`
	State string `json:"state"`
	LastUpdownSec float64 `json:"last_updown_sec"`
	Stats struct {
		Prefixes struct {
			Sent float64 `json:"sent"`
			Received float64 `json:"received"`
		} `json:"prefixes"`
		Message struct {
			Sent struct {
				Open float64 `json:"open"`
				Notifications float64 `json:"notifications"`
				Updates float64 `json:"updates"`
				Keepalives float64 `json:"keepalives"`
				RouteRefresh float64 `json:"route_refresh"`
			} `json:"sent"`
			Received struct {
				Open float64 `json:"open"`
				Notifications float64 `json:"notifications"`
				Updates float64 `json:"updates"`
				Keepalives float64 `json:"keepalives"`
				RouteRefresh float64 `json:"route_refresh"`
			} `json:"received"`
		} `json:"message"`
		Update struct {
			Sent struct {
				Updates float64 `json:"updates"`
				Withdraws float64 `json:"withdraws"`
				EOR float64 `json:"eor"`
			} `json:"sent"`
			Received struct {
				Updates float64 `json:"updates"`
				Withdraws float64 `json:"withdraws"`
				EOR float64 `json:"eor"`
			} `json:"received"`
		} `json:"update"`
		RouteRefresh struct {
			Sent struct {
				Request float64 `json:"request"`
				BORR float64 `json:"borr"`
				EORR float64 `json:"eorr"`
			} `json:"sent"`
			Received struct {
				Request float64 `json:"request"`
				BORR float64 `json:"borr"`
				EORR float64 `json:"eorr"`
			} `json:"received"`
		} `json:"route-refresh"`
	} `json:"stats"`
}

var (
	listenAddress = flag.String("web.listen-address", ":9179",
		"Address on which to expose metrics and web interface")
	metricsPath = flag.String("web.telemetry-path", "/metrics",
		"Path under which to expose metrics")
	bgpdsock = flag.String("bgpd.socket", "/var/run/bgpd.sock.0",
		"Path to the OpenBGPd control socket")

	bgpLabels = []string{"remote_as", "description", "remote_addr", "af"}

	// Metrics
	up = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "up"),
		"Was the last bgpctl query successful",
		nil, nil,
	)
	peerState = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "peer_state"),
		"State of a neighbor (-1 = Unknown, 0 = Idle, 1 = Connect, 2 = Active, 3 = OpenSent, 4 = OpenConfirm, 5 = Established)",
		bgpLabels, nil,
	)
	peerTime = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "peer_time"),
		"Seconds since last neighbor state change",
		bgpLabels, nil,
	)
	peerAdvPrefixes = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "peer_prefixes_advertised"),
		"Number of prefixes advertised to a neighbor",
		bgpLabels, nil,
	)
	peerRcvdPrefixes = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "peer_prefixes_received"),
		"Number of prefixes received from a neighbor",
		bgpLabels, nil,
	)
	peerSentMessages = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "peer_messages_sent"),
		"Number of BGP messages sent to a neighbor",
		append(bgpLabels, "type"), nil,
	)
	peerRcvdMessages = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "peer_messages_received"),
		"Number of BGP messages received from a neighbor",
		append(bgpLabels, "type"), nil,
	)
	peerSentUpdates = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "peer_updates_sent"),
		"Number of updates/withdraw sent to a neighbor",
		append(bgpLabels, "type"), nil,
	)
	peerRcvdUpdates = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "peer_updates_received"),
		"Number of updates/withdraw received from a neighbor",
		append(bgpLabels, "type"), nil,
	)
	peerSentRouteRefresh = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "peer_route_refresh_sent"),
		"Number of RR messages sent to a neighbor",
		append(bgpLabels, "type"), nil,
	)
	peerRcvdRouteRefresh = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "peer_route_refresh_received"),
		"Number of RR messages received from a neighbor",
		append(bgpLabels, "type"), nil,
	)
)

func NewExporter() *Exporter {
	return &Exporter{ }
}

func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
	ch <- up
	ch <- peerState
	ch <- peerTime
	ch <- peerAdvPrefixes
	ch <- peerRcvdPrefixes
	ch <- peerSentMessages
	ch <- peerRcvdMessages
	ch <- peerSentUpdates
	ch <- peerRcvdUpdates
	ch <- peerSentRouteRefresh
	ch <- peerRcvdRouteRefresh
}

func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	bgpneighbors, err := e.BgpctlRun()
	if err != nil {
		ch <- prometheus.MustNewConstMetric(
			up, prometheus.GaugeValue, 0,
		)
		log.Println(err)
		return
	}
	ch <- prometheus.MustNewConstMetric(
		up, prometheus.GaugeValue, 1,
	)

	e.UpdateMetrics(bgpneighbors.Neighbors, ch)
}

func (e *Exporter) GetPeerState(state string) (float64) {
	if state == "Idle" {
		return 0
	} else if state == "Connect" {
		return 1
	} else if state == "Active" {
		return 2
	} else if state == "OpenSent" {
		return 3
	} else if state == "OpenConfirm" {
		return 4
	} else if state == "Established" {
		return 5
	} else {
		return -1
	}
}

func (e *Exporter) UpdateMetrics(neighbors []bgpNeighbors, ch chan<- prometheus.Metric) {
	for _, neighbor := range neighbors {
		ip, err := netaddr.ParseIP(neighbor.RemoteAddr)
		if (err != nil) {
			pfx, errpfx := netaddr.ParseIPPrefix(neighbor.RemoteAddr)
			if (errpfx != nil) {
				log.Println("Unable to determine peer address family")
				return
			}
			ip = pfx.IP()
		}
		var af string
		if ip.Is4() {
			af = "4"
		} else if ip.Is6() {
			af = "6"
		}

		ch <- prometheus.MustNewConstMetric(peerTime, prometheus.GaugeValue, neighbor.LastUpdownSec, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af)
		ch <- prometheus.MustNewConstMetric(peerState, prometheus.GaugeValue, e.GetPeerState(neighbor.State), neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af)
		ch <- prometheus.MustNewConstMetric(peerAdvPrefixes, prometheus.GaugeValue, neighbor.Stats.Prefixes.Sent, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af)
		ch <- prometheus.MustNewConstMetric(peerRcvdPrefixes, prometheus.GaugeValue, neighbor.Stats.Prefixes.Received, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af)
		// Messages
		ch <- prometheus.MustNewConstMetric(peerSentMessages, prometheus.GaugeValue, neighbor.Stats.Message.Sent.Open, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "open")
		ch <- prometheus.MustNewConstMetric(peerSentMessages, prometheus.GaugeValue, neighbor.Stats.Message.Sent.Notifications, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "notifications")
		ch <- prometheus.MustNewConstMetric(peerSentMessages, prometheus.GaugeValue, neighbor.Stats.Message.Sent.Updates, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "updates")
		ch <- prometheus.MustNewConstMetric(peerSentMessages, prometheus.GaugeValue, neighbor.Stats.Message.Sent.Keepalives, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "keepalives")
		ch <- prometheus.MustNewConstMetric(peerSentMessages, prometheus.GaugeValue, neighbor.Stats.Message.Sent.RouteRefresh, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "route_refresh")
		ch <- prometheus.MustNewConstMetric(peerRcvdMessages, prometheus.GaugeValue, neighbor.Stats.Message.Received.Open, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "open")
		ch <- prometheus.MustNewConstMetric(peerRcvdMessages, prometheus.GaugeValue, neighbor.Stats.Message.Received.Notifications, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "notifications")
		ch <- prometheus.MustNewConstMetric(peerRcvdMessages, prometheus.GaugeValue, neighbor.Stats.Message.Received.Updates, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "updates")
		ch <- prometheus.MustNewConstMetric(peerRcvdMessages, prometheus.GaugeValue, neighbor.Stats.Message.Received.Keepalives, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "keepalives")
		ch <- prometheus.MustNewConstMetric(peerRcvdMessages, prometheus.GaugeValue, neighbor.Stats.Message.Received.RouteRefresh, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "route_refresh")
		// Updates
		ch <- prometheus.MustNewConstMetric(peerSentUpdates, prometheus.GaugeValue, neighbor.Stats.Update.Sent.Updates, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "updates")
		ch <- prometheus.MustNewConstMetric(peerSentUpdates, prometheus.GaugeValue, neighbor.Stats.Update.Sent.Withdraws, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "withdraws")
		ch <- prometheus.MustNewConstMetric(peerSentUpdates, prometheus.GaugeValue, neighbor.Stats.Update.Sent.EOR, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "eor")
		ch <- prometheus.MustNewConstMetric(peerRcvdUpdates, prometheus.GaugeValue, neighbor.Stats.Update.Received.Updates, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "updates")
		ch <- prometheus.MustNewConstMetric(peerRcvdUpdates, prometheus.GaugeValue, neighbor.Stats.Update.Received.Withdraws, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "withdraws")
		ch <- prometheus.MustNewConstMetric(peerRcvdUpdates, prometheus.GaugeValue, neighbor.Stats.Update.Received.EOR, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "eor")
		// RR
		ch <- prometheus.MustNewConstMetric(peerSentRouteRefresh, prometheus.GaugeValue, neighbor.Stats.RouteRefresh.Sent.Request, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "request")
		ch <- prometheus.MustNewConstMetric(peerSentRouteRefresh, prometheus.GaugeValue, neighbor.Stats.RouteRefresh.Sent.BORR, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "borr")
		ch <- prometheus.MustNewConstMetric(peerSentRouteRefresh, prometheus.GaugeValue, neighbor.Stats.RouteRefresh.Sent.EORR, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "eorr")
		ch <- prometheus.MustNewConstMetric(peerRcvdRouteRefresh, prometheus.GaugeValue, neighbor.Stats.RouteRefresh.Received.Request, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "request")
		ch <- prometheus.MustNewConstMetric(peerRcvdRouteRefresh, prometheus.GaugeValue, neighbor.Stats.RouteRefresh.Received.BORR, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "borr")
		ch <- prometheus.MustNewConstMetric(peerRcvdRouteRefresh, prometheus.GaugeValue, neighbor.Stats.RouteRefresh.Received.EORR, neighbor.RemoteAS, neighbor.Description, neighbor.RemoteAddr, af, "eorr")
	}
}

func (e *Exporter) BgpctlRun() (*bgpCtl, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	cmd := exec.CommandContext(ctx, "/usr/sbin/bgpctl", "-s", *bgpdsock, "-j", "show", "neighbor")

	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	err := cmd.Run()
	if err != nil {
		return nil, fmt.Errorf("command %s failed: %w: stderr: %s: stdout: %s", cmd, err, strings.Replace(stderr.String(), "\n", " ", -1), strings.Replace(stdout.String(), "\n", " ", -1))
	}

	var bgpctl bgpCtl
	if err := json.Unmarshal([]byte(stdout.String()), &bgpctl); err != nil {
		return nil, err
	}

	return &bgpctl, nil
}

func main() {

	promlogConfig := &promlog.Config{}
	flag.Parse()
	logger := promlog.New(promlogConfig)

	exporter := NewExporter()
	prometheus.MustRegister(exporter)

	level.Info(logger).Log("msg", "Starting obgpd_exporter", "version", version.Info())
	level.Info(logger).Log("msg", "Build context", "build_context", version.BuildContext())
	if user, err := user.Current(); err == nil && user.Uid == "0" {
		level.Warn(logger).Log("msg", "OpenBGPd Exporter is running as root user. This exporter is designed to run as unpriviledged user, root is not required.")
	}

	http.Handle(*metricsPath, promhttp.Handler())
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
             <head><title>OpenBGPd Exporter</title></head>
             <body>
             <h1>OpenBGPd Exporter</h1>
             <p><a href='` + *metricsPath + `'>Metrics</a></p>
             </body>
             </html>`))
	})
	level.Info(logger).Log("msg", "Listening on", "address", *listenAddress)
	log.Fatal(http.ListenAndServe(*listenAddress, nil))
}
