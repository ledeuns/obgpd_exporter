# OpenBGPd Exporter for Prometheus

Export [OpenBGPd](https://www.openbgpd.org/) neighbors statistics to [Prometheus](https://prometheus.io).

Metrics are retrieved using the JSON export of the `bgpctl` command.

To run it :

    go build
    ./obgpd_exporter [flags]

## Exported metrics
| Metric | Description | Labels |
| ------ | ------- | ------ |
| obgpd_up | Was the last bgpctl query successful | |
| obgpd_peer_time | Seconds since last neighbor state change | remote_as, description, remote_addr, af |
| obgpd_peer_state | State of a neighbor (-1 = Unknown, 0 = Idle, 1 = Connect, 2 = Active, 3 = OpenSent, 4 = OpenConfirm, 5 = Established) | remote_as, description, remote_addr, af |
| obgpd_peer_prefixes_advertised | Number of prefixes advertised to a neighbor | remote_as, description, remote_addr, af |
| obgpd_peer_prefixes_received | Number of prefixes received from a neighbor | remote_as, description, remote_addr, af |
| obgpd_peer_messages_sent | Number of BGP messages sent to a neighbor | remote_as, description, remote_addr, af, type |
| obgpd_peer_messages_received | Number of BGP messages received from a neighbor | remote_as, description, remote_addr, af, type |
| obgpd_peer_updates_sent | Number of updates/withdraw sent to a neighbor | remote_as, description, remote_addr, af, type |
| obgpd_peer_updates_received | Number of updates/withdraw received from a neighbor | remote_as, description, remote_addr, af, type |
| obgpd_peer_route_refresh_sent | Number of RR messages sent to a neighbor | remote_as, description, remote_addr, af, type |
| obgpd_peer_route_refresh_received | Number of RR messages received from a neighbor | remote_as, description, remote_addr, af, type |

## Flags
    ./obgpd_exporter --help

| Flag | Description | Default |
| ---- | ----------- | ------- |
| web.listen-address | Address on which to expose metrics and web interface | `:9179` |
| web.telemetry-path | Path under which to expose metrics | `/metrics` |

## Notice
This exporter is inspired by the [Mirth Channel Exporter](https://github.com/teamzerolabs/mirth_channel_exporter). See the included LICENSE file for terms and conditions.
